<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class QuienesSomosController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:QuienesSomos:quienes_somos.html.twig');
    }
    
     public function profesionalesAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:QuienesSomos:quienes_somos_profesionales.html.twig');
    }
}
