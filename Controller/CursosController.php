<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CursosController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:inicio.html.twig');
    }

   public function reunionesProductivasAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:reuniones_mas_productivas.html.twig');
    }

   public function formacionAseguraFuturoAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:formacion_asegura_futuro.html.twig');
    }
    
    public function negociarNoImponerAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:negociar_no_imponer.html.twig');
    }

    public function creandoEquipoAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:creando_equipo.html.twig');
    }	

    public function lideresParaCambioAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:lideres_para_cambio.html.twig');
    }

  public function motivacionPorProyectoAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:motivacion_por_proyecto.html.twig');
    }

public function poderDeVozAction(){
	 return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:poder_de_voz.html.twig');
    }
}
