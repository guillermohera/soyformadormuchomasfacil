<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CursosImpartidosController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:CursosImpartidos:cursos_impartidos.html.twig');
    }
}
