<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Muchomasfacil\SoyformadorWebBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Muchomasfacil\SoyformadorWebBundle\Form\ContactoType;

class ContactoController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:Contacto:contacto.html.twig');
    }
 
    public function formularioAction(Request $request){
	
	  // create a task and give it some dummy data for this example
           $task = new Task();
           $task->setNombre('Escriba su nombre y apellidos: ');
           $task->setEmail(' Escriba su email: ');
           $task->setConsulta('Escriba el contenido de su consulta');
           $form = $this->createForm(new ContactoType(), $task);
	   if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                // perform some action, such as saving the task to the database
               $message = \Swift_Message::newInstance()
                    ->setSubject('Hello Email')
                    ->setFrom($task->getEmail())
                    ->setTo($this->container->getParameter('mailer_user'))
                    ->setBody(
                       $this->renderView(
                              'MuchomasfacilSoyformadorWebBundle:Contacto:contenidoCorreoContacto.html.twig', array('nombre' => $task->getNombre(), 'email'=> $task->getEmail(), 'contenidoConsulta' => $task->getConsulta())
                        )
                     )
               ;
               $this->get('mailer')->send($message);
                  return $this->render('MuchomasfacilSoyformadorWebBundle:Cursos:inicio.html.twig');
            }
    	}

        return $this->render('MuchomasfacilSoyformadorWebBundle:Contacto:formulario_contacto.html.twig', array(
            'form' => $form->createView(),
        ));

	
    }

   public function datosContactoAction(){
	return $this->render('MuchomasfacilSoyformadorWebBundle:Contacto:datos_contacto.html.twig');
    }
}
