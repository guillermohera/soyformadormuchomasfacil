<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AvisoLegalController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:AvisoLegal:aviso_legal.html.twig');
    }
}
