<?php

namespace Muchomasfacil\SoyformadorWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ComoLoHacemosController extends Controller
{
    public function indexAction()
    {
        return $this->render('MuchomasfacilSoyformadorWebBundle:ComoLoHacemos:como_lo_hacemos.html.twig');
    }
}
