<?php

namespace Muchomasfacil\SoyformadorWebBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class ContactoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* some code ... */
	$builder->setAttribute('show_legend', false);

        $builder->add('nombre', 'text');
        $builder->add('email', 'email');
	$builder->add('consulta','textarea');
   }

   /* more code ... */

   public function getName(){
	return '';
   }
}
