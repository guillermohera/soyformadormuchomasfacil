<?php

namespace Muchomasfacil\SoyformadorWebBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Reuniones productivas', array('route' => '_muchomasfacil_soyformador_web_curso_reuniones_productivas'));

	$menu->addChild('La formación asegura futuro.', array('route' => '_muchomasfacil_soyformador_web_curso_formacion_asegura_futuro'));

	$menu->addChild('Negociar no es imponer: busquemos los intereses comunes', array('route' => '_muchomasfacil_soyformador_web_curso_negociar_no_imponer'));

	$menu->addChild('Creando equipo.', array('route' => '_muchomasfacil_soyformador_web_curso_creando_equipo'));

	$menu->addChild('Líderes para el cambio.', array('route' => '_muchomasfacil_soyformador_web_curso_lideres_para_cambio'));

	$menu->addChild('Motivación por el proyecto', array('route' => '_muchomasfacil_soyformador_web_curso_motivacion_por_proyecto'));

	$menu->addChild('Éxito en las ponencias empresariales. El poder de la voz.', array('route' => '_muchomasfacil_soyformador_web_curso_poder_de_voz'));

       
        // ... add more children

        return $menu;
    }

    public function menuBar(FactoryInterface $factory, array $options){

	 $menu = $factory->createItem('root');

	 $menu->addChild('Cursos', array('route' => '_muchomasfacil_soyformador_web_homepage'));

	$menu->addChild('Quienes somos', array('route' => '_muchomasfacil_soyformador_web_quienes_somos_profesionales'));

	$menu->addChild('Cómo lo hacemos', array('route' => '_muchomasfacil_soyformador_web_como_lo_hacemos'));

	$menu->addChild('Cursos impartidos', array('route' => '_muchomasfacil_soyformador_web_cursos_impartidos'));

	$menu->addChild('Contacto', array('route' => '_muchomasfacil_soyformador_web_contacto_formulario'));

	$menu->addChild('Aviso legal', array('route' => '_muchomasfacil_soyformador_web_aviso_legal'));
	
	return $menu;
    }
}
