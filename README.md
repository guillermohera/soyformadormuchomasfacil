
#Instalación

La correcta configuración e instalación de este bundle en un framework Symfony2 permite acceder a la web http://stage.soyformador.es. 



La instalación del bundle requiere de los siguientes pasos:

##Configuración de composer.json:

Establecer nuestro bundle dentro de require, para posteriormente instalar sus dependencias:

	 "require": {
	             "guillermohera/soyformadormuchomasfacil": "dev-master"
	             ...
	             },	
	
Establecer una nueva entrada dentro de repositories. De esta manera decimos a composer.json en qué repositorio buscar el bundle añadido en el require:

   
	"repositories": [
     	       {
     		"type": "vcs",
     		 "url":  "https://guillermohera@bitbucket.org/guillermohera/soyformadormuchomasfacil.git"
     		 }
    		 ],

       
Después, establecemos:

	 "minimum-stability": "dev".

Por último, instalamos los cambios mediante composer install.


##Configuración de config.yml

Modificar el apartado Assetic Configuration, incluyendo el filtro lessphp

	assetic:
        debug:          "%kernel.debug%"
        use_controller: false
        bundles:        [ ]
        #java: /usr/bin/java
        filters:
         lessphp:
                 file: %kernel.root_dir%/../vendor/leafo/lessphp/lessc.inc.php
                 apply_to: "\.less$"
         cssrewrite: ~
	 ...


Revisar que el apartado Swiftmailer contenga las siguientes líneas:

	swiftmailer:
        transport: "%mailer_transport%"
        encryption: "%mailer_encryption%"
        #auth_mode: "%login%"
        host:      "%mailer_host%"
        username:  "%mailer_user%"
        password:  "%mailer_password%"
        #spool:     { type: memory }


##Configurar AppKernel.php, añadiendo al array de bundles los especificados a continuación:

    
        	public function registerBundles()
       			 {
       			  $bundles = array(
       			 new Knp\Bundle\MenuBundle\KnpMenuBundle(),
       			 new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
       			 new Muchomasfacil\SoyformadorWebBundle\MuchomasfacilSoyformadorWebBundle(),
      			  ...
     
	
## Configurar parameters.yml

El formulario de contacto permite enviar datos a un correo electrónico. Es necesario configurar los apartados mailer:host, mailer_user y mailer_password a gusto del usuario.
	
	       	parameters:
        		  mailer_transport:  smtp
        		  mailer_encryption: ssl
         		  mailer_host:       smtp.gmail.com
        		  mailer_user:       gui.a4b@gmail.com
         		  mailer_password:  pass

##Disfrute de su aplicación



  
	
