<?php

namespace Muchomasfacil\SoyformadorWebBundle\Entity;

class Task
{
    protected $nombre;

    protected $email;
    
    protected $consulta;

    public function getNombre()
    {
        return $this->nombre;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function getConsulta(){
        return $this->consulta;
    }
    
    public function setConsulta($consulta){
        $this->consulta = $consulta;
    }
}

?>
